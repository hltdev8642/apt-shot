# APT-SHOT Screenshot App Previews Utility   
  Usage:   
    `apt-shot [appname]`

  Config:   
    `apt-shot [config|cfg|c]`

  Clear cached screenshots:   
    `apt-shot [clean|cln]`
